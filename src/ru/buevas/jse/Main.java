package ru.buevas.jse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import ru.buevas.jse.model.Circle;
import ru.buevas.jse.model.Rectangle;
import ru.buevas.jse.model.Shape;
import ru.buevas.jse.model.Square;

public class Main {

    public static void main(String[] args) {
        Collection<Circle> circles = new ArrayList<>();
        addToCollection(circles, new Circle(3.0), new Circle(10.0));
        System.out.println("Sum areas for circles: " + getAreasSum(circles));

        Collection<Rectangle> rectangles = new ArrayList<>();
        addToCollection(rectangles, new Rectangle(3.0, 5.0), new Rectangle(10.0, 100.0));
        System.out.println("Sum areas for rectangles: " + getAreasSum(rectangles));

        Collection<Square> squares = new ArrayList<>();
        addToCollection(squares, new Square(3.0), new Square(10.0));
        System.out.println("Sum areas for squares: " + getAreasSum(squares));

        Collection<Shape> shapes = new ArrayList<>();
        addToCollection(shapes, new Circle(3.0), new Rectangle(3.0, 5.0), new Square(10.0));
        System.out.println("Sum areas for shapes: " + getAreasSum(shapes));
    }

    public static <T extends Shape> void addToCollection(Collection<T> collection, T... shapes) {
        collection.addAll(Arrays.asList(shapes));
    }

    public static double getAreasSum(Collection<? extends Shape> collection) {
        return collection.stream()
                .mapToDouble(Shape::getArea)
                .sum();
    }
}
