package ru.buevas.jse.model;

public abstract class Shape {

    public abstract Double getArea();
}
