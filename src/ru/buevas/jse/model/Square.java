package ru.buevas.jse.model;

public class Square extends Shape {

    private Double side;

    public Square(Double side) {
        this.side = side;
    }

    @Override
    public Double getArea() {
        return side * side;
    }
}
